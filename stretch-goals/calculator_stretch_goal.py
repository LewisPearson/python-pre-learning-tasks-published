def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        answer = a + b
    if operator == "-":
        answer = a - b
    if operator == "*":
        answer = a * b
    if operator == "/":
        answer  = a / b


    binary = ""
    while answer != 0:
        remainder = int(answer % 2)
        binary = str(remainder) + str(binary)
        answer = int(answer) /2
        answer = int(answer)

    return binary
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
